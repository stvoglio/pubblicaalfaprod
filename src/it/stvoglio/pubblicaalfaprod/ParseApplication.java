package it.stvoglio.pubblicaalfaprod;

import android.app.Application;

import com.parse.Parse;
import com.parse.Parse.Configuration;
import com.parse.ParseInstallation;
import com.parse.PushService;

public class ParseApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		// Add your initialization code here
		// Parse.initialize(this, <applicationId>, <ClientID>);
		
		// Parse.initialize(this, "vwjzi0jmAEkzr6epxNgloSKFWWXsjoxa5su8DtcC", "uXDEoHyJGGFnMnsjIYI14AfsqMNrFB5a4GS4UAS8");
		
		// per puntare alla seconda app su Parse				
		// Parse.initialize(this, "npdbBwJROEgmR9MyaPELIonswiO1NyIz7TwgsOn8", "Yc8nl97YuVP3pU4lSgNcV1aG4KFC11Teo2EhvBH9");
		
		// per puntare alla app "DB20160324" su Parse		
		Parse.initialize(this, "UWhOFXuKLI5lJL1acVpG9QMIZsQVgCyRFHCmq57p", "tIovNDjiSPKlQd2PPvYq7QGL7j4ciGWlqEQkmgZg");

		/*
		Parse my = null;
		my.initialize(new Parse.Configuration.Builder(this)
				.applicationId("npdbBwJROEgmR9MyaPELIonswiO1NyIz7TwgsOn8").clientKey(null)
				//.server("http://bitnami-parse-test.northeurope.cloudapp.azure.com:1337/parse/") // The trailing slash is important
				.server("http://bitnami-parse-test.northeurope.cloudapp.azure.com/parse/").build());
		*/
		
		/*
		Parse.initialize(new Parse.Configuration.Builder(this)
				.applicationId("npdbBwJROEgmR9MyaPELIonswiO1NyIz7TwgsOn8")
				// should correspond to APP_ID env variable
				.clientKey("Yc8nl97YuVP3pU4lSgNcV1aG4KFC11Teo2EhvBH9")
				// set explicitly unless clientKey is explicitly configured on Parse server				
				.server("https://bitnami-parse-test.northeurope.cloudapp.azure.com/parse/").build());
		*/
		// PushService.setDefaultPushCallback(this, MainActivity.class);

		ParseInstallation installation = ParseInstallation.getCurrentInstallation();
		installation.saveInBackground();
	}
}