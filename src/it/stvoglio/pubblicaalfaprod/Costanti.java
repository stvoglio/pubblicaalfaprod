package it.stvoglio.pubblicaalfaprod;

public class Costanti {
	
	// static final String PREFS_NAME = "FilePreferenze_iTrading";
	static final String PREFS_NAME = "FilePreferenze_PubblicaAlfaProd";
	static final String versionePreferenzeRegistrata = "2016-03-26";
	
	// profile su parse
	final static String ACQUISTIANDROID_orderId = "orderId";
	final static String ACQUISTIANDROID_productId = "productId";
	final static String ACQUISTIANDROID_purchaseTime = "purchaseTime";
	final static String ACQUISTIANDROID_purchaseState = "purchaseState";
	final static String ACQUISTIANDROID_developerPayload = "develolperPayload";
	final static String ACQUISTIANDROID_purchaseToken = "purchaseToken";
	final static String ACQUISTIANDROID_deviceId = "deviceId";
	final static String ACQUISTIANDROID_userId = "userId";
	
	final static String PARSE_CLASS_ACQUISTIANDROID = "AcquistiAndroid";

}
