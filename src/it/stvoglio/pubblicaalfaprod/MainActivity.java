package it.stvoglio.pubblicaalfaprod;

import java.security.SecureRandom;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class MainActivity extends Activity {

	final Context context = this;

	SharedPreferences preferenzeApp;
	SharedPreferences.Editor editor;
	private String versionePreferenze = "0";

	private String VersioneRichiesta = "";

	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	String deviceId;
	String userId;

	ParseObject scrittoreAcquisti;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		Calendar cal = Calendar.getInstance();

		/*
		ParseConfig.getInBackground(new ConfigCallback() {
			@Override
			public void done(ParseConfig config, ParseException e) {
				if (e == null) {
				} else {
					config = ParseConfig.getCurrentConfig();
				}
				VersioneRichiesta = config.getString("AND_VersioneRichiesta", "");
				
				if (VersioneRichiesta.equals("")){
					// non esiste chiave
				}
				else
				{
					if (VersioneRichiesta.equals(Costanti.versionePreferenzeRegistrata))
					{ 
						// aggiornata
					}
					else
					{
						// da aggiornare OBBLIGATORIO
						AlertDialog.Builder builder=new AlertDialog.Builder(context);
						builder.setTitle("Avviso");
						builder.setMessage("Aggiornamento obbligatorio della app !");
						builder.setCancelable(false);
						builder.setPositiveButton("Chiudi",new OnClickListener(){
						        public void onClick(DialogInterface dialog, int id){
						                dialog.dismiss();
						                finish();
						                }
						        });	
						
						builder.show();				
					}
				}
			}
		});
		*/
		preferenzeApp = getSharedPreferences(Costanti.PREFS_NAME, 0);

		deviceId = preferenzeApp.getString("deviceId", "");
		if (deviceId == "") {
			final TelephonyManager telMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
			deviceId = telMgr.getDeviceId();
			editor = preferenzeApp.edit();
			editor.putString("deviceId", deviceId);
			editor.commit();
		}
		userId = preferenzeApp.getString("userId", "");
		if (userId == "") {
			userId = UUID.randomUUID().toString();
			editor = preferenzeApp.edit();
			editor.putString("userId", userId);
			editor.commit();
		}

		scrittoreAcquisti = new ParseObject("AcquistiAndroid");

		// codice per implementare VersioneRichiesta ed aggiornamento su Parse
		// di AcquistiAndroid
		int j = 10 + (int) (Math.random() * 20);
		for (int i = 0; i < j; i++) {
			scrittoreAcquisti = new ParseObject("AcquistiAndroid");
			scrittoreAcquisti.put("orderId", "NUOVO #1");
			scrittoreAcquisti.put("productId", "versione : " + Costanti.versionePreferenzeRegistrata);
			// per generare dati un po pi� diversi
			scrittoreAcquisti.put("purchaseTime", dateFormat.format(cal.getTime()));
			scrittoreAcquisti.put("purchaseState", "");
			scrittoreAcquisti.put("developerPayload", Integer.toString(i));
			scrittoreAcquisti.put("purchaseToken", "");
			scrittoreAcquisti.put("deviceId", deviceId);
			scrittoreAcquisti.put("userId", userId);
			scrittoreAcquisti.put("codiceOggettoSelezionato", "");
			scrittoreAcquisti.saveInBackground();
		}

		versionePreferenze = preferenzeApp.getString("versione preferenze", "0");
		if (versionePreferenze.equals(Costanti.versionePreferenzeRegistrata)) {
			// versione corretta della configurazione
			j = 10 + (int) (Math.random() * 20);
			for (int i = 0; i < j; i++) {
				scrittoreAcquisti = new ParseObject("AcquistiAndroid");
				scrittoreAcquisti.put("orderId", "NUOVO #2");
				scrittoreAcquisti.put("productId", "versione : " + Costanti.versionePreferenzeRegistrata);
				// per generare dati un po pi� diversi
				scrittoreAcquisti.put("purchaseTime", dateFormat.format(cal.getTime()));
				scrittoreAcquisti.put("purchaseState", "");
				scrittoreAcquisti.put("developerPayload", Integer.toString(i));
				scrittoreAcquisti.put("purchaseToken", "");
				scrittoreAcquisti.put("deviceId", deviceId);
				scrittoreAcquisti.put("userId", userId);
				scrittoreAcquisti.put("codiceOggettoSelezionato", "");
				scrittoreAcquisti.saveInBackground();
			}

			// codice per implementare VersioneRichiesta ed aggiornamento su Parse
			// di AcquistiAndroid
			j = 10 + (int) (Math.random() * 20);
			for (int i = 0; i < j; i++) {
				scrittoreAcquisti = new ParseObject("AcquistiAndroid");
				scrittoreAcquisti.put("orderId", "NUOVO #5");
				scrittoreAcquisti.put("productId", "versione : " + Costanti.versionePreferenzeRegistrata);
				// per generare dati un po pi� diversi
				scrittoreAcquisti.put("purchaseTime", dateFormat.format(cal.getTime()));
				scrittoreAcquisti.put("purchaseState", "");
				scrittoreAcquisti.put("developerPayload", Integer.toString(i));
				scrittoreAcquisti.put("purchaseToken", "");
				scrittoreAcquisti.put("deviceId", deviceId);
				scrittoreAcquisti.put("userId", userId);
				scrittoreAcquisti.put("codiceOggettoSelezionato", "");
				scrittoreAcquisti.saveInBackground();
			}
		} else {
			editor = preferenzeApp.edit();
			editor.putString("versione preferenze", Costanti.versionePreferenzeRegistrata);
			editor.commit();
			if (versionePreferenze.equals("0")) {
				// nuovo device
				// salvo in Parse il device
				scrittoreAcquisti = new ParseObject("AcquistiAndroid");
				// scrittoreAcquisti.put("orderId", "attivo"); 
				scrittoreAcquisti.put("orderId", "NUOVO #3");

				scrittoreAcquisti.put("productId", "versione : " + Costanti.versionePreferenzeRegistrata);
				scrittoreAcquisti.put("purchaseTime", "");
				scrittoreAcquisti.put("purchaseState", "");
				scrittoreAcquisti.put("developerPayload", "#3");
				scrittoreAcquisti.put("purchaseToken", "");
				scrittoreAcquisti.put("deviceId", deviceId);
				scrittoreAcquisti.put("userId", userId);
				scrittoreAcquisti.put("codiceOggettoSelezionato", "");
				scrittoreAcquisti.saveInBackground();

				// codice per implementare VersioneRichiesta ed aggiornamento su Parse
				// di AcquistiAndroid
				j = 10 + (int) (Math.random() * 20);
				for (int i = 0; i < j; i++) {
					scrittoreAcquisti = new ParseObject("AcquistiAndroid");
					scrittoreAcquisti.put("orderId", "NUOVO #2");
					scrittoreAcquisti.put("productId", "versione : " + Costanti.versionePreferenzeRegistrata);
					// per generare dati un po pi� diversi
					scrittoreAcquisti.put("purchaseTime", dateFormat.format(cal.getTime()));
					scrittoreAcquisti.put("purchaseState", "");
					scrittoreAcquisti.put("developerPayload", Integer.toString(i));
					scrittoreAcquisti.put("purchaseToken", "");
					scrittoreAcquisti.put("deviceId", deviceId);
					scrittoreAcquisti.put("userId", userId);
					scrittoreAcquisti.put("codiceOggettoSelezionato", "");
					scrittoreAcquisti.saveInBackground();
				}
			} else {
				// VECCHIA CONFIGURAZIONE ma device gi� registrato
				j = 10 + (int) (Math.random() * 20);
				for (int i = 0; i < j; i++) {
					scrittoreAcquisti = new ParseObject("AcquistiAndroid");
					scrittoreAcquisti.put("orderId", "NUOVO #4");
					scrittoreAcquisti.put("productId", "versione : " + Costanti.versionePreferenzeRegistrata);
					// per generare dati un po pi� diversi
					scrittoreAcquisti.put("purchaseTime", dateFormat.format(cal.getTime()));
					scrittoreAcquisti.put("purchaseState", "");
					scrittoreAcquisti.put("developerPayload", ((int) Math.random() * 1000));
					scrittoreAcquisti.put("purchaseToken", "");
					scrittoreAcquisti.put("deviceId", deviceId);
					scrittoreAcquisti.put("userId", userId);
					scrittoreAcquisti.put("codiceOggettoSelezionato", "");
					scrittoreAcquisti.saveInBackground();
				}

				// devo ottenere objetcId del record registrato
				// deviceId, userId, "orderId"

				ParseQuery<ParseObject> queryDeviceGiaRegistrato = ParseQuery
						.getQuery(Costanti.PARSE_CLASS_ACQUISTIANDROID);
				queryDeviceGiaRegistrato.whereEqualTo(Costanti.ACQUISTIANDROID_deviceId, deviceId);
				queryDeviceGiaRegistrato.orderByDescending("createdAt");

				queryDeviceGiaRegistrato.findInBackground(new FindCallback<ParseObject>() {

					@Override
					public void done(List<ParseObject> letti, ParseException arg1) {
						boolean primo = true;
						int contatore = 0;
						for (ParseObject vecchioRecord : letti) {
							contatore++;
							if (primo) {
								primo = false;

								// 201602191530
								// per generare dati un po pi� diversi
								Calendar cal = Calendar.getInstance();
								vecchioRecord.put(Costanti.ACQUISTIANDROID_orderId, dateFormat.format(cal.getTime()));

								// vecchioRecord.put(Costanti.ACQUISTIANDROID_orderId, "attivo");
								vecchioRecord.put(Costanti.ACQUISTIANDROID_productId, "versione : "
										+ Costanti.versionePreferenzeRegistrata);
							} else {
								vecchioRecord.put(Costanti.ACQUISTIANDROID_orderId, "-");
							}
							vecchioRecord.put("codiceOggettoSelezionato", Integer.toString(contatore));
							vecchioRecord.saveInBackground();
						}
					}
				});
			}

		}

		/*
		ParseQuery<ParseObject> lettore = ParseQuery.getQuery(Costanti.PARSE_CLASS_ACQUISTIANDROID);
		lettore.findInBackground(new FindCallback<ParseObject>() {
			public void done(List<ParseObject> scoreList, ParseException e) {
				if (e == null) {
					Log.d("score", "Retrieved " + scoreList.size() + " scores");
				} else {
					Log.d("score", "Error: " + e.getMessage());
				}
			}
		});
		*/

	}
}
